import signal
import threading
import time

import isso_launcher
import marmot_launcher


def main():
    signal.signal(signal.SIGTERM, marmot_launcher.signal_handler)
    signal.signal(signal.SIGINT, marmot_launcher.signal_handler)
    signal.signal(signal.SIGHUP, marmot_launcher.signal_handler)

    marmot = threading.Thread(target=marmot_launcher.run_marmot)
    isso = threading.Thread(target=isso_launcher.run_isso)

    isso.start()
    time.sleep(1.0)
    marmot.start()

    isso.join()
    marmot.join()

    while True:
        time.sleep(10)


if __name__ == '__main__':
    main()
