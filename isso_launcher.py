import subprocess
import hashlib
from isso.db import SQLite3
import isso.config as isso_config

import config

_ISSO_CFG_PATH = "./isso.cfg"


def digest(string):
  hasher = hashlib.sha256()
  hasher.update(string.encode('utf-8'))
  return hasher.hexdigest()


def generate_isso_config():
    config_content = f"""
[general]
dbpath = {config.DB_PATH}
name = {config.APP_NAME}
host =
    https://{config.CORS_HOST}/
    http://{config.CORS_HOST}/
notify = stdout

[server]
listen = http://0.0.0.0:8080/
public-endpoint = https://{config.SERVE_HOST}

[guard]
enabled = true
ratelimit = 2
direct-reply = 3
reply-to-self = false
require-author = false
require-email = true

[admin]
enabled = true
password = {config.PASSWORD}
"""

    with open(_ISSO_CFG_PATH, "w") as f:
        f.write(config_content)

    ## Turns out every node generates it's own `session-key`
    ## In order to make everything work flawlessly you need 
    ## to screw with this key in DB so that cookies work 
    ## on any node. Using SHA256 of ADMIN_PASS should be
    ## good enough
    conf = isso_config.load(isso_config.default_file(), _ISSO_CFG_PATH)
    db = SQLite3(conf.get('general', 'dbpath'), conf)
    db.execute("INSERT OR REPLACE INTO preferences (key, value) VALUES (?, ?)", ("session-key", digest(config.PASSWORD)))

def run_isso():
    generate_isso_config()
    launch_cli = ['isso', '-c', _ISSO_CFG_PATH, 'run']
    p = subprocess.Popen(launch_cli, cwd="/root")
    p.wait()
