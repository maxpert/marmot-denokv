FROM frolvlad/alpine-glibc:alpine-3.17

ARG MARMOT_VERSION=v0.8.4

WORKDIR /root
COPY . .

ADD https://github.com/maxpert/marmot/releases/download/${MARMOT_VERSION}/marmot-${MARMOT_VERSION}-linux-amd64.tar.gz /tmp/marmot.tar.gz

RUN apk --no-cache add ca-certificates gcc g++ make python3 py3-pip python3-dev sqlite htop && \
    pip install isso gevent && \
    apk del python3-dev gcc g++ make

RUN mkdir -p /tmp/marmot && \
    cd /tmp/marmot && \
    tar vxzf /tmp/marmot.tar.gz && \
    mv /tmp/marmot/marmot /root/marmot && \
    cd /root && \
    rm -rf /tmp/marmot.tar.gz && \
    rm -rf /tmp/marmot


CMD ["python", "start.py"]
