import re
import signal
import subprocess
import sys
import time
import zlib

import config

_MARMOT_PATH = "/root/marmot"
_MARMOT_CFG_PATH = "/root/marmot-config.toml"
_MARMOT_PROC: subprocess.Popen or None = None

def get_fly_node_id() -> int:
    return int(zlib.crc32(config.PRIVATE_IP.encode('utf-8')) & 0x7FFFFFFF)


def generate_marmot_config():
    node_id = get_fly_node_id()
    config_content = f"""
db_path="{config.DB_PATH}"
seq_map_path="/root/seq-map.cbor"
node_id={node_id}

[replication_log]
shards=1
replicas=2
max_entries=1024
compress=true

[logging]
format="console"
    """

    if config.S3_ENDPOINT and config.S3_BUCKET and config.S3_PATH:
        config_content = config_content + f"""
[snapshot]
enable=true
store='s3'
interval=3600_000

[snapshot.s3]
bucket="{config.S3_BUCKET}"
endpoint="{config.S3_ENDPOINT}"
path="{config.S3_PATH}"
use_ssl=true
"""
    elif config.WEBDAV_URL:
        config_content = config_content + f"""
[snapshot]
enable=true
store='webdav'
interval=3600_000

[snapshot.webdav]
url="{config.WEBDAV_URL}"
        """
    else:
        config_content = config_content + f"""
[snapshot]
enable=false
        """

    with open(_MARMOT_CFG_PATH, "w") as f:
        f.write(config_content)


# noinspection PyUnresolvedReferences
def signal_handler(sig, frame):
    if _MARMOT_PROC is not None:
        _MARMOT_PROC.send_signal(sig)

    if sig == signal.SIGTERM or sig == signal.SIGINT:
        print("Received exit signal...")
        sys.exit(0)


def run_marmot():
    global _MARMOT_PROC

    generate_marmot_config()
    while True:
        launch = [
            _MARMOT_PATH,
            "-config", _MARMOT_CFG_PATH,
            "-cluster-addr", f"[{config.PRIVATE_IP}]:6222",
            "-cluster-peers", f"dns://global.{config.APP_NAME}.internal:6222/"
        ]

        print("Launching Marmot...")
        print(" ".join(launch))
        _MARMOT_PROC = subprocess.Popen(launch, cwd="/root")
        while _MARMOT_PROC.poll() is None:
            time.sleep(1.0)


if __name__ == "__main__":
    run_marmot()
