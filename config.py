import os
import secrets
import string


def random_password(length: int) -> string:
    chars = string.ascii_letters + string.digits
    return ''.join(secrets.choice(chars) for i in range(length))


ALLOC_ID = os.environ.get("FLY_ALLOC_ID")
APP_NAME = os.environ.get("FLY_APP_NAME")
WEBDAV_URL = os.environ.get('WEBDAV_URL')
PRIVATE_IP = os.environ.get("FLY_PRIVATE_IP")
DB_PATH = f"/root/comments.db" if ALLOC_ID is not None else "/tmp/comments.db"
CONFIG_PATH = "/root" if ALLOC_ID is not None else "."
PASSWORD = os.environ["ADMIN_PASS"] if os.environ.get("ADMIN_PASS") else random_password(12)
SERVE_HOST = f"{APP_NAME}.fly.dev" if APP_NAME else "localhost:8080"
CORS_HOST = f"{os.environ.get('CORS_HOST', SERVE_HOST)}"

# Endpoint of your S3 storage e.g. s3-us-east-1.amazonaws.com or s3.us-east-005.backblazeb2.com (for Blackblaze)
S3_ENDPOINT = os.environ.get('S3_ENDPOINT')
S3_BUCKET = os.environ.get('S3_BUCKET')
S3_PATH = os.environ.get('S3_PATH')
