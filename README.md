## What is Marmot?

Marmot is a distributed SQLite replicator that runs as a side-car to you service, and replicates data across
cluster using embedded NATS. Marmot relies on [JetStream](https://docs.nats.io/nats-concepts/jetstream)
based CDC (Change Data Capture) to replicate changes. JetStream under the hood uses RAFT for consensus
and allows for eventually consistent SQLite replicas (multi-primary replication).

## What is Isso?

[Isso](https://isso-comments.de/) is a commenting system like Disqus. Users can edit or delete own
comments (within 15 minutes by default). Best part about Isso is that it uses SQLite!


## What is Fly.io?

Fly is a platform for running full stack apps and databases close to your users. Compute jobs at Fly.io are
virtualized using Firecracker, the virtualization engine developed at AWS as the
engine for Lambda and Fargate.

## So What does this repo do?

This repository pairs Marmot with Isso for Fly.io. Currently [this page](https://maxpert.github.io/marmot/demo)
embedds the commenting system provided by this repo.


## How can I deploy one for myself?

Clone this repo, create your fly app, then:

```
fly deploy -a <app_name>
```

The Python scripts of repo automatically pick app application name and generate configuration files for Isso and Marmot.
Isso needs two environment variables `ADMIN_PASS` that is password for admin control panel, and `CORS_HOST` which enables
Isso to serve cross site requests (in case of https://www.example.com it will be www.example.com). You can set these via:

```
fly secrets set ADMIN_PASS=... CORST_HOST=...
```

Then you can go to your page on the domain configured above and drop script tags like:

```
<section id="isso-thread">
    <noscript>Javascript needs to be activated to view comments.</noscript>
</section>

<script data-isso="//<app_name>.fly.dev/" src="//<app_name>.fly.dev/js/embed.min.js"></script>
```

You can enable snapshots with WebDAV (I will add S3 configuration if enough people want it) using `WEBDAV_URL`. For the format
of the URL checkout https://github.com/maxpert/marmot/blob/master/config.toml `snapshot.webdav.url`. Just like `ADMIN_PASS` 
you can set it via:

```
fly secrets set WEBDAV_URL=...
```

**NOTICE** For free version of Fly.io instance it's not recommended to use NATS snapshot storage, as it requires more memory and
it can cause OOM

## Using with S3 backup

Set following environment variables for using with S3
 - `S3_ENDPOINT` to S3 region endpoint e.g. s3-us-east-1.amazonaws.com or s3.us-east-005.backblazeb2.com (for Blackblaze)
 - `S3_BUCKET` name of bucket
 - `S3_PATH` path of snapshots in bucket
 - `AWS_ACCESS_KEY` or `AWS_ACCESS_KEY_ID` for access key
 - `AWS_SECRET_KEY` or `AWS_SECRET_ACCESS_KEY` for secret key

 